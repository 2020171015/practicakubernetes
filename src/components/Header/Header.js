import React from 'react';
import { Avatar, Space } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import './Header.css';



const Header = () => {
    return (
        <Space direction="vertical" size={16} >
            <Space wrap size={16}>
                <Avatar size={88} icon={<UserOutlined />} />
                <h1>Dulce</h1>

            </Space>

        </Space>
    );
}

export default Header;