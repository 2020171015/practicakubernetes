import React, { useState, useEffect } from 'react';
import { Form, Input, Button } from 'antd';
import Axios from 'axios';
import './NoteForm.css';
import Header from '../Header/Header';
import ListItem from '../List/List';

const NoteForm = () => {
    const [notes, setNotes] = useState([]);
    const [form] = Form.useForm(); // Crear una instancia del formulario

    useEffect(() => {
        async function obtenerNotas() {
            try {
                const respuesta = await Axios.get('http://localhost:3000/api/note');
                setNotes(respuesta.data);
            } catch (error) {
                console.error('Error al obtener las notas desde el server:', error);
            }
        }

        obtenerNotas();
    }, []);

    const onFinish = async (values) => {
        try {
            const respuesta = await Axios.post('http://localhost:3000/api/note', {
                title: values.title,
                text: values.note,
            });
            const nuevaNota = respuesta.data;
            setNotes([...notes, nuevaNota]);
            console.log('Nota enviada ...:', values);
            form.resetFields(); // Limpiar los campos del formulario
        } catch (error) {
            console.error('Error al guardar la nota:', error);
        }
    };

    return (
        <>
            <div className='container'>
                <Header />

                <Form form={form} name="note-form" onFinish={onFinish} className='Form'
                    labelCol={{ flex: '200px' }}
                    labelAlign="left"
                    labelWrap
                    wrapperCol={{ flex: 1 }}
                    colon={false}
                    style={{ maxWidth: 800, marginTop: '30px' }}
                >
                    <Form.Item
                        name="title"
                        rules={[
                            {
                                required: true,
                                message: 'Por favor, ingresa tu nombre',
                            },
                        ]}
                    >
                        <Input placeholder="Nombre" />
                    </Form.Item>

                    <Form.Item
                        name="note"
                        rules={[
                            {
                                required: true,
                                message: 'Por favor, ingresa el contenido de tu publicación',
                            },
                        ]}
                    >
                        <Input placeholder="Post" />
                    </Form.Item>

                    <Form.Item style={{ textAlign: 'right' }}>
                        <Button type="primary" htmlType="submit">
                            Publicar
                        </Button>
                    </Form.Item>
                </Form>
                <ListItem notes={notes} />
            </div>
        </>
    );
};

export default NoteForm;
