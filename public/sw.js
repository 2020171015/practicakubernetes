//Importar el archivo de configuracion

importScripts('js/sw-utils.js')

//Crear las variables de cache
const CACHE_DYNAMIC = 'dynamic-v1' //Para los archivos que se van a descargar
const CACHE_STATIC = 'static-v1'    //App shell
const CACHE_INMUTABLE = 'inmutable-v1'// CDN de terceros. LIBRERIAS

const limpiarCache = (cacheName, numberItem) => {
    caches.open(cacheName)
        .then(cache => {
            cache.keys()
                .then(keys => {
                    if (keys.length > numberItem) {
                        cache.delete(keys[0])
                            .then(limpiarCache(cacheName, numberItem))
                    }
                })
        })

}
self.addEventListener('install', function (event) {

    const cahePromise = caches.open(CACHE_STATIC).then(function (cache) {

        return cache.addAll([

            '/',
            '/index.html',
            '/js/app.js',
            '/js/sw-utils.js',
            '/sw.js',
            'static/js/bundle.js',
            'not-found.jpeg',
            'manifest.json',


        ])
    })
    const caheInmutable = caches.open(CACHE_INMUTABLE).then(function (cache) {

        return cache.addAll([

            'https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Roboto:wght@100&display=swap',


        ])
    })
    event.waitUntil(Promise.all([cahePromise, caheInmutable]))
})
//Funcion para eliminar cache anterior
self.addEventListener('activate', function (event) {
    const respuesta = caches.keys()
        .then(keys => {
            keys.forEach(key => {

                if (key !== CACHE_STATIC && key.includes('static')) {
                    return caches.delete(key)
                }
            })
        })
    event.waitUntil(respuesta)
})

//Estrategia de cache
self.addEventListener('fetch', event => {

    const respuesta = caches.match(event.request, { caché: "no-store" }).then(res => {
        if (res) {

            actualizaCacheStatico(CACHE_STATIC, event.request, CACHE_INMUTABLE);
            return res;
        } else {

            return fetch(event.request).then(newRes => {

                return actualizaCacheDinamico(CACHE_DYNAMIC, event.request, newRes);

            });

        }

    });

    event.respondWith(respuesta)
})